//
//  SchoolGameView.swift
//  you-can-think
//
//  Created by Lee Irvine on 2/22/20.
//  Copyright © 2020 kezzi.co. All rights reserved.
//

import UIKit

class SchoolGameView: UIView {

    var game: SchoolGame?
        
    var scale: CGFloat = 1.0
    
    var pan: CGPoint = CGPoint.zero
    
    override func draw(_ rect: CGRect) {
        let scaleTransform = CGAffineTransform(scaleX: self.scale, y: self.scale)

        let transform = scaleTransform.translatedBy(x: self.pan.x, y: self.pan.y)
        
        if let walls = self.game?.walls {
            walls.forEach { wall in
                let path = UIBezierPath(rect: wall.rect.applying(transform))
                
                wall.color.set()
                
                path.fill()
            }
        }

        
        if let player = self.game?.player {
            let radius = player.radius
            
            let bbox = CGRect(x: player.origin.x - radius, y: player.origin.y - radius,
                              width: radius * 2.0, height: radius * 2.0)
            
            let path = UIBezierPath(ovalIn: bbox.applying(transform))
            
            UIColor.red.set()
            
            path.fill()
        }

        
    }

}
