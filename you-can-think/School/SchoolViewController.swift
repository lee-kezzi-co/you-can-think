//
//  SchoolViewController.swift
//  you-can-think
//
//  Created by Lee Irvine on 2/22/20.
//  Copyright © 2020 kezzi.co. All rights reserved.
//

import UIKit

class SchoolViewController: UIViewController {

    var game = SchoolGame()

    @IBOutlet weak var gameView: SchoolGameView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let gameWidth = CGFloat(480.0)
        
        let gameHeight = (UIScreen.main.bounds.size.height / UIScreen.main.bounds.size.width) * gameWidth
        
        self.gameView.scale = UIScreen.main.bounds.size.width / gameWidth
        
        self.gameView.game = self.game
        
        self.gameView.pan = CGPoint(
            x: self.game.player.origin.x * -1.0 + gameWidth/2.0,
            y: self.game.player.origin.y * -1.0 + gameHeight/2.0)
    }
    
    
    
    
}
