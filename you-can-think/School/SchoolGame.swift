//
//  SchoolGame.swift
//  you-can-think
//
//  Created by Lee Irvine on 2/22/20.
//  Copyright © 2020 kezzi.co. All rights reserved.
//

import UIKit

class SchoolWall: NSObject {
    let color: UIColor
    
    let rect: CGRect
    
    init(rect: CGRect, color: UIColor = UIColor(rgb: 0xB5B9E3)) {
        self.rect = rect
        
        self.color = color
        
        super.init()
    }
}

class SchoolPlayer: NSObject {
    var origin: CGPoint = CGPoint.zero
    
    var radius: CGFloat = 22.0
    
    init(origin: CGPoint = CGPoint.zero) {
        self.origin = origin
    }
}

class SchoolGame: NSObject {
    var walls: [SchoolWall] = [
        SchoolWall(rect: CGRect(origin: CGPoint(x: 60, y: 60), size: CGSize(width: 120, height: 120))),
        SchoolWall(rect: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 480, height: 20))),
        SchoolWall(rect: CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 20, height: 480)))
    ]
    
    var player: SchoolPlayer = SchoolPlayer(origin: CGPoint(x: 240, y: 240))
}
