//
//  KeyboardLayoutConstraint.swift
//  lendme
//
//  Created by Lee Irvine on 8/28/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit

class KeyboardLayoutConstraint: NSLayoutConstraint {

    private var initialValue: CGFloat!
        
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setup()
    }
    
    fileprivate func setup() {
        
        NotificationCenter.default.addObserver(self, selector: #selector(willShowKeyboard(_:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willHideKeyboard(_:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        self.initialValue = self.constant
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func willShowKeyboard(_ note :Notification) {
        guard let keyboardRect = note.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as? CGRect else {
            return
        }
        
        self.constant = self.initialValue + keyboardRect.size.height
        self.animate()
    }

    @objc func willHideKeyboard(_ note :Notification) {
        self.constant = self.initialValue
        self.animate()
    }

    private func animate() {
        // is this broken?
        guard let view = UIApplication.shared.keyWindow?.subviews.first else {
            return
        }
        
        UIView.animate(withDuration: 0.2) {
            view.layoutIfNeeded()
        }
    }
 
}

extension UIView {
    @discardableResult func pinBottomToKeyboard() -> KeyboardLayoutConstraint {
        guard let superview = self.superview else {
            preconditionFailure("view has no superview")
        }

        self.translatesAutoresizingMaskIntoConstraints = false
        
        let constraint = KeyboardLayoutConstraint(item: self,
                                            attribute: .bottom,
                                            relatedBy: .equal,
                                            toItem: superview,
                                            attribute: .bottom,
                                            multiplier: 1,
                                            constant: 0)
        
        constraint.priority = UILayoutPriority.defaultHigh
        superview.addConstraint(constraint)
        
        return constraint
    }
    
//    @discardableResult func pinKeyboard(edge: Edge,
//                                to otherEdge: Edge,
//                                of view: UIView,
//                                constant: CGFloat = 0,
//                                priority: UILayoutPriority = UILayoutPriority.required,
//                                relatedBy relation: NSLayoutRelation = .equal) -> NSLayoutConstraint {
//        guard let superview = self.superview else {
//            preconditionFailure("view has no superview")
//        }
//
//        translatesAutoresizingMaskIntoConstraints = false
//        if view !== superview {
//            view.translatesAutoresizingMaskIntoConstraints = false
//        }
//
//        let constraint = KeyboardLayoutConstraint(item: self,
//                                            attribute: edge.layoutAttribute,
//                                            relatedBy: relation,
//                                            toItem: view,
//                                            attribute: otherEdge.layoutAttribute,
//                                            multiplier: 1,
//                                            constant: constant)
//        constraint.priority = priority
//        superview.addConstraint(constraint)
//
//        constraint.setup()
//
//        return constraint
//    }

}
