//
//  VectorView.swift
//  you-can-think
//
//  Created by Lee Irvine on 8/24/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class VectorView: UIView {
    
    let bgView: UIImageView
    
    init(image name: String) {
        let image = UIImage(named: name)
        
        self.bgView = UIImageView(image: image, highlightedImage: nil)
        
        self.bgView.contentMode = .scaleAspectFit
        
        super.init(frame: CGRect.zero)

        self.addSubview(self.bgView)

        self.bgView.pinToSuperviewEdges()

        self.backgroundColor = .clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
