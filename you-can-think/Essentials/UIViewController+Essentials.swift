//
//  UIViewController+Essentials.swift
//
//  Created by Lee Irvine on 8/30/18.
//  Copyright © 2018 kezzi.co. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func hideKeyboardOnTap() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func alert(_ title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func confirm(_ title: String, message: String, completion:@escaping (Bool) -> ()) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)

        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in completion(true) }))
        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { _ in completion(false) }))
        
        self.present(alert, animated: true)
    }

    @objc func backButtonClicked(){
        self.navigationController?.popViewController(animated: true)
    }

}
