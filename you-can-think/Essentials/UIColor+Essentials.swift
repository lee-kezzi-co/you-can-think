//
//  UIColor+Essentials.swift
//  dog-breed
//
//  Created by Lee Irvine on 7/30/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(rgb: Int) {
        let red = CGFloat((rgb >> 16) & 0xFF) / 255
        let green = CGFloat((rgb >> 8) & 0xFF) / 255
        let blue = CGFloat(rgb & 0xFF) / 255
        
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }

    convenience init(rgb: Int, alpha: CGFloat) {
        let red = CGFloat((rgb >> 16) & 0xFF) / 255
        let green = CGFloat((rgb >> 8) & 0xFF) / 255
        let blue = CGFloat(rgb & 0xFF) / 255
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
