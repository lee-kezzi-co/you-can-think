//
//  ThinkLabel.swift
//  you-can-think
//
//  Created by Lee Irvine on 8/27/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class ThinkLabel: UILabel {

    init(text: String, size: CGFloat = 22.0) {
        super.init(frame: CGRect.zero)
        
        self.text = text
        
        self.font = UIFont(name: "American Typewriter", size: size)
        
        self.textAlignment = .center
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
