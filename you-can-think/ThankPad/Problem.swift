//
//  Problem.swift
//  you-can-think
//
//  Created by Lee Irvine on 10/31/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class Problem: NSObject {
    let equation: String
    
    let solution: Int
    
    var answer: Int?
    
    var p0: Int
    
    var p1: Int
    
    var sign: Int
    
    override init() {
        let sign = (Int.random(in: 0...1) == 1) ? 1 : -1

        var p = [ Int.random(in: 0..<10), Int.random(in: 0..<10) ]
        
        if sign < 0 {
            p.sort()
            
            p.reverse()
        }
        
//        let p0 = Int.random(in: 0..<10)
        
//        let p1 = Int.random(in: 0..<10)
        
        self.solution = p[0] + p[1] * sign
        
        self.equation = "\(p[0]) \(sign < 0 ? "-" : "+") \(p[1]) ="
        
        self.p0 = p[0]
        
        self.p1 = p[1]
        
        self.sign = sign
        
        super.init()
    }
    
    
}
