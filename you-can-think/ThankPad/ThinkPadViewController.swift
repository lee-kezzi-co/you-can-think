//
//  ThinkPadViewController.swift
//  you-can-think
//
//  Created by Lee Irvine on 8/24/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

class ThinkPadViewController: UIViewController {

    weak var questionView: VectorView!

    weak var bodyView: VectorView!

    weak var entryView: VectorView!
    
    @IBOutlet weak var problemTitleLabel: UILabel!
    
    @IBOutlet weak var problemLabel: UILabel!
    
    @IBOutlet weak var answerLabel: UILabel!
    
    @IBOutlet weak var okButton: ThinkButton!

    var answer: String = ""
    
    var problems: [Problem]!
    
    var problemIndex:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.problems = [ Problem() ]
        
        self.update()
        
        self.askNextProblem()
    }
    
    func update() {
        let problem = self.problems[problemIndex]
        
        self.answerLabel.text = answer
        
        self.problemLabel.text = problem.equation
        
        self.problemTitleLabel.text = "SOLVE MATH Q\(problemIndex+1)"
    }
}

extension ThinkPadViewController : ThinkButtonDelegate {
    func didTapOkayButton() {
        guard self.answer.isEmpty == false else { return }
        
        let problem = self.problems[problemIndex]

        problem.answer = Int(self.answer)

        self.problemIndex = problemIndex + 1
        
        self.problems.append(Problem())

        if problem.answer == problem.solution {
            let congrats = [ "Good", "GotIt", "Great", "Incredible"]
            
            let randomCongrats = Int.random(in: 0..<congrats.count)
            
            self.okButton.isEnabled = false
            
            Baldi.speak([ congrats[randomCongrats] ]) {
                self.askNextProblem()
                
                self.okButton.isEnabled = true
                
                self.update()
            }
        } else {
            
            let imageView = UIImageView()
            
            imageView.backgroundColor = UIColor.black
            
            imageView.image = UIImage(named: "baldi-angry")
            
            imageView.contentMode = .scaleAspectFit
            
            imageView.isUserInteractionEnabled = true
            
            self.view.addSubview(imageView)
            
            imageView.pinEdges(to: self.view)
            
            Baldi.speak([ "Screech" ]) {
                imageView.removeFromSuperview()
                
                self.askNextProblem()
            
                self.update()
                
//                Baldi.speak([ "Ruler" ])
            }
        }
        
        
    }
    
    func askNextProblem() {
        let problem = self.problems[problemIndex]

        let sign = problem.sign > 0 ? "Plus" : "Minus"
        
        Baldi.speak(["Problem\(problemIndex+1)", "\(problem.p0)", sign, "\(problem.p1)", "Equals"])
        
        self.answer = ""

    }
    
    func didTapClearButton() {
        self.answer = ""
        
        self.update()
    }
    
    func didTapNumberButton(_ value: String) {
        if value == "-" && answer.contains("-") {
            self.answer = answer.replacingOccurrences(of: "-", with: "")
            
        } else if value == "-" && self.answer.contains("-") == false {
            self.answer = "-\(answer)"
            
        } else {
            self.answer = "\(answer)\(value)"
            
        }
        
        self.update()
    }
}
