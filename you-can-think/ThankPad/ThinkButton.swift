//
//  ThinkButton.swift
//  you-can-think
//
//  Created by Lee Irvine on 8/24/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit

@objc protocol ThinkButtonDelegate : NSObjectProtocol {
    func didTapNumberButton(_ value : String)
    func didTapOkayButton()
    func didTapClearButton()
}

class ThinkButton: UIView {
    
    let defaultColor = UIColor(rgb: 0x88208C)
    
    let highlightColor = UIColor(rgb: 0xD332D9)
    
    @IBOutlet weak var delegate: ThinkButtonDelegate?
    
    weak var image: VectorView!
    
    weak var label: UILabel!
    
    var isEnabled: Bool = true

    override func awakeFromNib() {
        super.awakeFromNib()

//        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(didTapButton(_:))))
        
        self.backgroundColor = .clear

        self.image.tintColor = defaultColor
        
        self.label.textColor = .white
    }

    @objc func didTapButton() {

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.image.tintColor = highlightColor
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.image.tintColor = defaultColor
        
        guard self.isEnabled else { return }
        
        self.didTapButton()
    }
    
}

class ThinkButtonOkay: ThinkButton {
    
    override func awakeFromNib() {
        let image = VectorView(image: "think-pad-circle")
        
        let label = ThinkLabel(text: "OK", size: 66.0)
        
        self.addSubview(image)
        
        self.addSubview(label)
        
        image.pinToSuperviewEdges()
        
        label.pinToSuperviewEdges()
        
        self.image = image
        
        self.label = label
        
        super.awakeFromNib()
    }
    
    override func didTapButton() {
        self.delegate?.didTapOkayButton()
    }
}

class ThinkButtonNumeric: ThinkButton {
    @IBInspectable var keyValue: String?
    
    override func awakeFromNib() {
        let image = VectorView(image: "think-pad-octagon")
        
        let label = ThinkLabel(text: self.keyValue ?? "<>", size: 33.0)
        
        self.addSubview(image)
        
        self.addSubview(label)
        
        image.pinToSuperviewEdges()
        
        label.pinToSuperviewEdges()
        
        self.image = image
        
        self.label = label

        self.image.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        
        self.image.layer.shadowOpacity = 1.0
        
        self.image.layer.shadowRadius = 0.0
        
        self.image.layer.shadowOffset = CGSize(width: -3, height: 3)
        
        self.image.layer.shadowColor = UIColor(rgb: 0x4B0548).cgColor

        super.awakeFromNib()
    }
    
    override func didTapButton() {
        guard let value = self.keyValue else { fatalError("key value missing") }
        
        self.delegate?.didTapNumberButton(value)
    }
}

class ThinkButtonClear: ThinkButtonNumeric {
    override func didTapButton() {
        self.delegate?.didTapClearButton()
    }
}
