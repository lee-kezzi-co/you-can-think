//
//  Baldi.swift
//  you-can-think
//
//  Created by Lee Irvine on 9/14/19.
//  Copyright © 2019 kezzi.co. All rights reserved.
//

import UIKit
import AVFoundation

class Baldi: NSObject {
    static var queue: AudioPlayerQueue?
    
    class func speak(_ words: [String], completion: @escaping ()->() = { }) {
        let urls = words.compactMap {
            return Bundle.main.url(forResource: "BAL_\($0)", withExtension: "mp3")
        }
        
        self.queue = AudioPlayerQueue(sounds: urls) {
            self.queue = nil
            
            completion()
            
        }
        
        queue?.playNext()
    }
}

internal class AudioPlayerQueue: NSObject {
    var soundUrls:[URL]
    
    var player: AVAudioPlayer!
    
    var completion: ()->() = { }

    init(sounds: [URL], completion:@escaping ()->()) {
        self.soundUrls = sounds
        
        self.completion = completion
        
        super.init()
    }
    
    func playNext() {
        guard soundUrls.count > 0 else { completion(); return }
        
        guard let url = soundUrls.first else { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
            try AVAudioSession.sharedInstance().setActive(true)
            
            /* The following line is required for the player to work on iOS 11. Change the file type accordingly*/
            player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileType.mp3.rawValue)
            
            /* iOS 10 and earlier require the following line:
             player = try AVAudioPlayer(contentsOf: url, fileTypeHint: AVFileTypeMPEGLayer3) */
            
            guard let player = player else { return }

            player.delegate = self

            player.play()
            
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
}

extension AudioPlayerQueue: AVAudioPlayerDelegate {
    func audioPlayerDecodeErrorDidOccur(_ player: AVAudioPlayer, error: Error?) {
        print("decode error")
    }
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        print("finished sound")
        
        soundUrls.remove(at: 0)

        playNext()
    }
}
